// SPDX-License-Identifier: MIT
// Copyright (c) 2015-2021 Zig Contributors, Ketan Reynolds (8Bitkitkat <ketan.cs.reynolds@gmail.com>)
// This file uses parts of [zig](https://ziglang.org/), which is MIT licensed.
// The MIT license requires this copyright notice to be included in all copies
// and substantial portions of the software.

const std = @import("std");
const mem = std.mem;
const Allocator = mem.Allocator;
const assert = std.debug.assert;

pub fn DynArray(comptime T: type) type {
    return DynArrayAligned(T, null);
}

pub fn DynArrayAligned(comptime T: type, comptime alignment: ?u29) type {
    if (alignment) |a| {
        if (a == @alignOf(T)) {
            return DynArrayAligned(T, null);
        }
    }
    return struct {
        const Self = @This();

        pub const items_t = if (alignment) |a| ([]align(a) T) else []T;
        pub const items_const_t = if (alignment) |a| ([]align(a) const T) else []const T;

        items: items_t,
        capacity: usize,
        allocator: *Allocator,

        /// Create a new DynArray
        pub fn init(allocator: *Allocator) Self {
            return Self{
                .items = &[_]T{},
                .capacity = 0,
                .allocator = allocator,
            };
        }

        /// Create a new DynArray with an already allocated size
        pub fn initWithCapacity(allocator: *Allocator, capacity: usize) !Self {
            var self = Self.init(allocator);

            const new_mem = try self.allocator.allocAdvanced(T, allignment, capacity, .at_least);
            self.items.ptr = new_mem.ptr;
            self.capacity = new_mem.len;

            return self;
        }

        /// Free a DynArray
        pub fn deinit(self: Self) void {
            self.allocator.free(self.getAllocatedMem());
        }

        /// Push an item
        pub fn push(self: *Self, item: T) !void {
            const new_item_ptr = try self.extendByOne();
            new_item_ptr.* = item;
        }

        /// Push an items assuming that there is enough capacty to hold it
        pub fn pushUnchekced(self: *Self, item: T) void {
            const new_item_ptr = self.extendByOneUnchecked();
            new_item_ptr.* = item;
        }

        /// Remove and return the last element from the array.
        /// Returns null if the array is empty.
        pub fn pop(self: *Self) ?T {
            return if (self.isEmpty())
                null
            else
                self.popUnchecked();
        }

        /// Remove and return the last element from the array.
        /// Assumes at least on item is in the array.
        pub fn popUnchecked(self: *Self) T {
            const ret = self.items[self.items.len - 1];
            self.items.len -= 1;
            return ret;
        }

        /// Removes an item at an index and returns it.
        /// Returns null if the array is empty
        pub fn remove(self: *Self, index: usize) ?T {
            return if (self.isEmpty())
                null
            else
                self.removeUnchecked();
        }

        /// Removes an item at an index and retuerns it.
        /// Assumes at lease on item is in the array.
        pub fn removeUnchecked(self: *Self, index: usize) T {
            const new_len = self.items.len - 1;
            if (new_len == 1)
                return self.popUnchecked();

            const old_item = self.items[i];
            for (self.items[i..new_len]) |*b, j|
                b.* = self.items[i + 1 + j];
            self.items[new_len] = undefined;
            self.items.len = new_len;
            return old_item;
        }

        /// Push an array to this one.
        pub fn pushArray(self: *Self, items: items_const_t) !void {
            try self.ensureCapcity(self.items.len + items.len);
            self.pushArrayUnchecked(items);
        }

        /// Push an array to this one,
        /// Assuming that there is enough capacty to hold it.
        pub fn pushArrayUnchecked(self: *Self, items: items_const_t) void {
            const old_len = self.items.len;
            const new_len = old_len + items.len;
            self.items.len = new_len;
            mem.copy(T, self.items[old_len..], items);
        }

        /// Insert an item to an index.
        pub fn insert(self: *Self, index: usize, item: T) !void {
            try self.ensureCapcity(self.items.len + 1);
            self.insertUnchecked(index, item);
        }

        /// Insert an item to an index,
        /// Assuming that there is enough capacty to hold it.
        pub fn insertUnchecked(self: *Self, index: usize, item: T) void {
            self.items.len += 1;
            mem.copyBackwards(T, self.items[index + 1 .. self.items.len], self.items[index .. self.items.len - 1]);
            self.items[index] = item;
        }

        /// Insert an array to an index.
        pub fn insertArray(self: *Self, index: usize, items: items_const_t) void {
            try self.ensureCapcity(self.items.len + items.len);
            self.insertArrayUncheck(index, items);
        }

        /// Insert an array to an index,
        /// Assuming that there is enough capacty to hold it.
        pub fn insertArrayUncheck(self: *Self, index: usize, items: items_const_t) void {
            self.items.len += items.len;
            mem.copyBackwards(T, self.items[index + items.len .. self.items.len], self.items[i .. self.items.len - items.len]);
            mem.copy(T, self.items[index .. index + items.len], items);
        }

        /// Convert to an `std.ArrayListAligned`,
        /// possibly buggy, needs more testing
        pub fn toArrayList(self: *Self) std.ArrayListAligned(T, allignment) {
            return .{ .items = self.items, .capacity = self.capacity, .allocator = self.allocator };
        }

        /// Returns `true` if `items.len` == 0
        pub fn isEmpty(self: *Self) bool {
            return self.items.len == 0;
        }

        /// Get the last item if not empty
        pub fn last(self: *Self) ?*T {
            return if (self.isEmpty())
                null
            else
                self.lastUnchecked();
        }

        /// Get the last item, assumes that there is at least one item
        pub fn lastUnchecked(self: *Self) *T {
            return &self.items[self.items.len - 1];
        }

        /// Get the first item if not empty
        pub fn first(self: *Self) ?*T {
            if (self.isEmpty())
                null
            else
                self.firstUnchecked();
        }

        /// Get the first item, assumes that there is at least one item
        pub fn firstUnchecked(self: *Self) *T {
            return &self.items[0];
        }

        /// Clear the whole array
        pub fn clear(self: *Self) void {
            self.getAllocatedMem() = mem.zeroes(items_t);
        }
        // =-=-=-=-=-=-= | Helper methods | =-=-=-=-=-=-=
        //

        /// Set the array len to the capacity, the new items will be `undefined`
        pub fn fillToCapacity(self: *Self) void {
            self.items.len = self.capacity;
        }

        /// Increase length by 1, returning pointer to the new item.
        /// The returned pointer becomes invalid when the list is resized.
        pub fn extendByOne(self: *Self) !*T {
            const new_len = self.items.len + 1;
            try self.ensureCapcity(new_len);
            return self.extendByOneUnchecked();
        }

        /// Increase length by 1, returning pointer to the new item.
        /// Asserts that there is already space for the new item without allocating more.
        /// The returned pointer becomes invalid when the list is resized.
        pub fn extendByOneUnchecked(self: *Self) *T {
            assert(self.items.len < self.capacity);

            self.items.len += 1;
            return &self.items[self.items.len - 1];
        }

        /// Ensure that the capacity is at least what is given
        pub fn ensureCapcity(self: *Self, new_capacity: usize) !void {
            var better_capacity = self.capacity;
            if (better_capacity >= new_capacity)
                return;

            while (true) {
                better_capacity += better_capacity / 2 + 8;
                if (better_capacity >= new_capacity)
                    break;
            }

            const new_mem = try self.allocator.reallocAtLeast(self.getAllocatedMem(), better_capacity);
            self.items.ptr = new_mem.ptr;
            self.capacity = new_mem.len;
        }

        /// Gets the allocated memory, this is not the length, for that use `items.len`.
        /// Any new items will be `undefined`
        fn getAllocatedMem(self: Self) items_t {
            return self.items.ptr[0..self.capacity];
        }
    };
}
