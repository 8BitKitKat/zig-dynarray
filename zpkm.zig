const api = @import(".zpkm/api.zig");

pub fn createManifest(b: *api.ManifestBuilder) void {
    b.addLib(.{
        .name = "DynArray",
        .entry_point = "DynArray.zig",
    });
}
