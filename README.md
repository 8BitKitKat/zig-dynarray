# DynArry

My own implementation of a dynamic array in zig.

I personaly think my implementation is more consistent than `std.ArrayList`,
although functionaly they are almost the same.

All methods are documentented.
